﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;
using System.Xml.Linq;

namespace TimeTracker
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window , INotifyPropertyChanged
    {
        protected override void OnClosing(CancelEventArgs e)
        {
            base.OnClosing(e);
            //e.Cancel = true;
        }

        const double DeltaHoursAgo = -6; //константа для сохранения во вчерашнюю дату до 6:00 сегодня

        //-события
        public RelayActionCommand TimePeopleCommand { get; set; }
        private void TimePeopleCommand_Execute(object obj)
        {
            ((MyTask)obj).TimeRun();
        }
        private bool TimePeopleCommand_CanExecute(object obj)
        {
            return true;
        }


        public RelayActionCommand TimePeoplePropertyCommand { get; set; }
        private void TimePeoplePropertyCommand_Execute(object obj)
        {
            LogTimeProperty frm = new LogTimeProperty((MyTask)obj);
            frm.Show();
        }
        private bool TimePeoplePropertyCommand_CanExecute(object obj)
        {
            return true;
        }


        /// <summary>
        /// Вызывается Событиями изменения каких либо Свойств элементов Коллекции
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Log_TimeStructure_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            savealllog();
            calc_and_save_time_totxt();
        }
        /// <summary>
        /// Событие изменения коллекции
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Log_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // если добавление
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                TimeStructure newel = e.NewItems[0] as TimeStructure;
                newel.PropertyChanged += this.Log_TimeStructure_PropertyChanged; //подписываемся на изменениея элементов коллекции
                savealllog();
                calc_and_save_time_totxt();
            }
        }
        private void Window_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            XDocument Xml = new XDocument(); XElement rootel = new XElement("position");  Xml.Add(rootel);

            XElement top = new XElement("top"); top.Value = this.Top.ToString(); rootel.Add(top);
            XElement left = new XElement("left"); left.Value = this.Left.ToString(); rootel.Add(left);
            XElement height = new XElement("height"); height.Value = this.Height.ToString(); rootel.Add(height);
            XElement width = new XElement("width"); width.Value = this.Width.ToString(); rootel.Add(width);

            Xml.Save("window.xml");
        }




        /// <summary>
        /// Коллекция задач - общение с людьми
        /// </summary>
        public ObservableCollection<MyHuman> Humans { get; }
        /// <summary>
        /// Коллекция задач
        /// </summary>
        public ObservableCollection<MyTask> Tasks { get; }



        DateTime timenow;
        /// <summary>
        /// Текущее время
        /// </summary>
        public DateTime TimeNow
        {
            get
            {
                return timenow;
            }
            set
            {
                if (timenow != value)
                {
                    timenow = value;
                    OnPropertyChanged();
                }
            }
        }


        public MainWindow()
        {
            //-команды
            TimePeopleCommand = new RelayActionCommand();
            TimePeopleCommand.CanExecuteAction += TimePeopleCommand_CanExecute;
            TimePeopleCommand.ExecuteAction += TimePeopleCommand_Execute;

            TimePeoplePropertyCommand = new RelayActionCommand();
            TimePeoplePropertyCommand.CanExecuteAction += TimePeoplePropertyCommand_CanExecute;
            TimePeoplePropertyCommand.ExecuteAction += TimePeoplePropertyCommand_Execute;


            //-проверка на повторяемость имен
            bool controltasksname(ObservableCollection<MyTask> tasks)
            {
                foreach (var el in tasks)
                {
                    if (tasks.Where(x => x.Name == el.Name).Count() > 1)
                    {
                        return false;
                    }
                }
                return true;
            }
            Humans = gethumans(); //получаем людей
            Tasks = gettasks(); //получаем задачи

            if (!controltasksname(new ObservableCollection<MyTask>(Humans.Select(x => (MyTask)x).ToList())) && !controltasksname(Tasks)) {
                MessageBox.Show("Повторяющиеся имена не допустимы!");
                Close();
            }
            


            //-подписывемся на изменения
            foreach (var el in this.Humans)
            {
                el.Log.CollectionChanged += Log_CollectionChanged; //подписывемся на изменения колекции
                foreach (var subel in el.Log)
                {
                    subel.PropertyChanged += Log_TimeStructure_PropertyChanged;
                }
            }
            foreach (var el in this.Tasks)
            {
                el.Log.CollectionChanged += Log_CollectionChanged; //подписывемся на изменения колекции
                foreach (var subel in el.Log)
                {
                    subel.PropertyChanged += Log_TimeStructure_PropertyChanged;
                }
            }
            //-подгружаем старое положение окна
            try
            {
                XDocument Xml = XDocument.Load("window.xml");
                Top = (double)Xml.Element("position").Element("top");
                Left = (double)Xml.Element("position").Element("left");
                Height = (double)Xml.Element("position").Element("height");
                Width = (double)Xml.Element("position").Element("width");
            }
            catch { }
            this.PropertyChanged += Window_PropertyChanged; //подписывемся на изменения колекции


            InitializeComponent();


            //-таймер для часов
            var timer = new DispatcherTimer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            timer.Start();
            var timer2 = new DispatcherTimer();
            timer2.Tick += new EventHandler(timer_1minit_Tick);
            timer2.Interval = new TimeSpan(0, 0, 0, 30);
            timer2.Start();
            timer_1minit_Tick(null, null);
        }
        /// <summary>
        /// Таймер
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_Tick(object sender, EventArgs e)
        {
            TimeNow = DateTime.Now;
        }
        /// <summary>
        /// Таймер 1 раз в миуту
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_1minit_Tick(object sender, EventArgs e)
        {
            try
            {
                var time = calc_and_save_time_totxt();
                string sss = String.Format("О: {0} + Р: {1} = {2}.", 
                    time.all_human_time.ToString(@"h\:mm"), 
                    time.all_task_time.ToString(@"h\:mm"), 
                    (time.all_human_time + time.all_task_time).ToString(@"h\:mm"));
                Title = sss;
            } catch { }
        }

        /// <summary>
        /// Получить лог времени
        /// </summary>
        /// <param name="Name"></param>
        /// <returns></returns>
        private ObservableCollection<TimeStructure> gettasklog(string Name, string roottasksname, string taskname)
        {
            var log = new ObservableCollection<TimeStructure>();
            string filepath = "log/" + (DateTime.Now.AddHours(DeltaHoursAgo)).ToShortDateString() + ".xml";
            if (System.IO.File.Exists(filepath))
            {
                XDocument Xml = XDocument.Load(filepath);
                if (Xml.Element("log").Element(roottasksname).Elements(taskname).FirstOrDefault(x => (string)x.Attribute("name") == Name) != null)
                {
                    IEnumerable<XElement> loghuman = (from el in Xml.Element("log").Element(roottasksname).Elements(taskname)
                                                      where (string)el.Attribute("name") == Name
                                                      select el); //так сойдет


                    foreach (var el in loghuman.Elements("time"))
                    {
                        if (el.Attribute("complite").Value.ToString() == "false") log.Add(new TimeStructure((DateTime)el.Attribute("starttime")));
                        else log.Add(new TimeStructure((DateTime)el.Attribute("starttime"), (DateTime)el.Attribute("endtime"), (string)el.Attribute("comments")));
                    }
                }
            }

            return log;
        }
        /// <summary>
        /// Получить людей
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<MyHuman> gethumans()
        {
            var arr = new ObservableCollection<MyHuman>();

            XDocument Xml = XDocument.Load("conf/peoples.xml");
            foreach (var el in Xml.Element("peoples").Elements("people"))
            {
                arr.Add(new MyHuman((string)el.Attribute("name"),
                    Environment.CurrentDirectory + "/conf/img/" + (string)el.Attribute("pic"),
                    gettasklog((string)el.Attribute("name"), "peoples", "people"))
                );
            }

            return arr;
        }
        /// <summary>
        /// Получить задачи
        /// </summary>
        /// <returns></returns>
        private ObservableCollection<MyTask> gettasks()
        {
            var arr = new ObservableCollection<MyTask>();

            XDocument Xml = XDocument.Load("conf/tasks.xml");
            foreach (var el in Xml.Element("tasks").Elements("task"))
            {
                var iscommunication = el.Attribute("iscommunication");
                var isrelaxation = el.Attribute("isrelaxation");
                if (iscommunication != null && (bool)iscommunication == true)
                {
                    arr.Add(
                        new MyCommunicationTask((string)el.Attribute("name"),
                        gettasklog((string)el.Attribute("name"), "tasks", "task")
                        )
                    );
                }
                else if (isrelaxation != null && (bool)isrelaxation == true)
                {
                    arr.Add(
                        new MyRelaxationTask((string)el.Attribute("name"),
                        gettasklog((string)el.Attribute("name"), "tasks", "task")
                        )
                    );
                }
                else
                {
                    arr.Add(
                        new MyTask((string)el.Attribute("name"),
                        gettasklog((string)el.Attribute("name"), "tasks", "task")
                        )
                    );
                }
            }

            return arr;
        }
        /// <summary>
        /// Сохранить все логи
        /// </summary>
        private void savealllog()
        {
            void logtimetoxml(XElement task, ObservableCollection<TimeStructure> tlog)
            {
                foreach (var time in tlog)
                {
                    XElement xmltime = new XElement("time"); task.Add(xmltime);
                    xmltime.Add(new XAttribute("complite", time.Complite));
                    xmltime.Add(new XAttribute("starttime", time.StartTime));
                    if (time.Complite)
                    {
                        xmltime.Add(new XAttribute("endtime", time.EndTime));
                        xmltime.Add(new XAttribute("comments", time.Comments));
                    }
                }
            }


            XDocument Xml = new XDocument();
            XElement log = new XElement("log"); Xml.Add(log);

            XElement peoples = new XElement("peoples"); log.Add(peoples);
            foreach (var el in Humans)
            {
                XElement people = new XElement("people"); peoples.Add(people);
                people.Add(new XAttribute("name", el.Name));

                logtimetoxml(people, el.Log);
            }

            XElement tasks = new XElement("tasks"); log.Add(tasks);
            foreach (var el in Tasks)
            {
                XElement task = new XElement("task"); tasks.Add(task);
                task.Add(new XAttribute("name", el.Name));

                logtimetoxml(task, el.Log);
            }

            Xml.Save("log/" + (DateTime.Now.AddHours(DeltaHoursAgo)).ToShortDateString() + ".xml");
        }
        /// <summary>
        /// Сохранить в txt
        /// </summary>
        private (TimeSpan all_human_time, TimeSpan all_task_time) calc_and_save_time_totxt()
        {
            TimeSpan all_communication_time = new TimeSpan();
            TimeSpan all_task_time = new TimeSpan();
            TimeSpan all_relax_time = new TimeSpan();

            using (StreamWriter sw = new StreamWriter("log/" + (DateTime.Now.AddHours(DeltaHoursAgo)).ToShortDateString() + ".txt", false, System.Text.Encoding.Default))
            {

                void logtimetotxt(ObservableCollection<TimeStructure> tlog)
                {
                    foreach (var time in tlog)
                    {
                        if (time.Complite)
                        {
                            sw.WriteLine(string.Format("{0} - {1} ({2})   {3}",
                                 time.StartTime.ToString(@"HH\:mm"), time.EndTime.ToString(@"HH\:mm"), (time.EndTime - time.StartTime).ToString(@"h\:mm"), time.Comments));
                        }
                        else
                        {
                            sw.WriteLine(string.Format("({0} - ... {1})  {2}",
                                 time.StartTime.ToString(@"HH\:mm"), (DateTime.Now - time.StartTime).ToString(@"h\:mm"), time.Comments));
                        }
                    }
                }

                //-считает общее время для данного лога
                TimeSpan calctasktime(ObservableCollection<TimeStructure> log)
                {
                    TimeSpan time=new TimeSpan();
                    foreach(var el in log)
                    {
                        time += (el.Complite) ? el.EndTime-el.StartTime : DateTime.Now - el.StartTime;
                    }

                    return time;
                }
                //-печатаем отчет для данного лога
                TimeSpan calc_and_printtask(MyTask el)
                {
                    TimeSpan time = new TimeSpan();
                    if (el.Log.Count > 0)
                    {
                        time = calctasktime(el.Log);

                        sw.WriteLine(el.Name + " " + time.ToString(@"h\:mm"));
                        logtimetotxt(el.Log);
                        sw.WriteLine();
                    }
                    return time;
                }

                foreach (var el in Humans)
                {
                    all_communication_time += calc_and_printtask(el);
                }
                foreach (var el in Tasks.Where(x => x is MyCommunicationTask))
                {
                    all_communication_time += calc_and_printtask(el);
                }
                sw.WriteLine("=Всего общение: " + all_communication_time.ToString(@"h\:mm"));
                sw.WriteLine("\n***************************************************");
                foreach (var el in Tasks.Where(x => !(x is MyCommunicationTask || x is MyRelaxationTask)))
                {
                    all_task_time += calc_and_printtask(el);
                }
                sw.WriteLine("=Всего задачи: " + all_task_time.ToString(@"h\:mm"));

                sw.WriteLine("\n==Общее время: " + (all_communication_time + all_task_time).ToString(@"h\:mm"));

                sw.WriteLine("\n***************************************************");
                foreach (var el in Tasks.Where(x => x is MyRelaxationTask))
                {
                    all_relax_time += calc_and_printtask(el);
                }

                sw.WriteLine("\n\n*Разговоры по времени*");
                timetracklog(sw);

            }


            return (all_communication_time, all_task_time);
        }
        private void timetracklog(StreamWriter sw)
        {
            var fulllog = new List<(DateTime timestart, TimeStructure parameter, MyTask task)>();
            void addtofulllog(MyTask task)
            {
                foreach (var el in task.Log)
                {
                    fulllog.Add((el.StartTime, el, task));
                }
            }
            foreach (var el in Humans)
            {
                addtofulllog(el);
            }
            foreach (var el in Tasks)
            {
                addtofulllog(el);
            }

            foreach(var el in fulllog.Where(x => x.task is MyHuman || x.task is MyCommunicationTask).OrderBy(x => x.timestart))
            {
                if (el.parameter.Complite)
                {
                    sw.WriteLine(string.Format("({2})   {0} - {1}   {4} {3}",
                         el.parameter.StartTime.ToString(@"HH\:mm"), el.parameter.EndTime.ToString(@"HH\:mm"), (el.parameter.EndTime - el.parameter.StartTime).ToString(@"h\:mm"), el.parameter.Comments, el.task.Name));
                }
                else
                {
                    sw.WriteLine(string.Format("({1})   {0} - ...   {3} {2}",
                         el.parameter.StartTime.ToString(@"HH\:mm"), (DateTime.Now - el.parameter.StartTime).ToString(@"h\:mm"), el.parameter.Comments, el.task.Name));
                }
            }
        }


        #region Реализация INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        
    }


    public class TimeStructure : INotifyPropertyChanged
    {
        bool complite;
        public bool Complite
        {
            get
            {
                return complite;
            }
            private set
            {
                if (complite != value)
                {
                    complite = value;
                    OnPropertyChanged();
                }
            }
        }

        DateTime starttime;
        public DateTime StartTime
        {
            get
            {
                return starttime;
            }
            set
            {
                if (starttime != value)
                {
                    starttime = value;
                    OnPropertyChanged();
                }
            }
        }

        DateTime endtime;
        public DateTime EndTime
        {
            get
            {
                return endtime;
            }
            set
            {
                if (endtime != value)
                {
                    endtime = value;
                    OnPropertyChanged();
                }
            }
        }


        string comments;
        public string Comments
        {
            get
            {
                return comments;
            }
            set
            {
                if (comments != value)
                {
                    comments = value;
                    OnPropertyChanged();
}
            }
        }


        public void End(DateTime EndTime)
        {
            this.EndTime = EndTime;
            this.Complite = true;
        }
        public void End(DateTime EndTime, string Comments)
        {
            this.EndTime = EndTime;
            this.Comments = Comments;
            this.Complite = true;
        }

        public TimeStructure(DateTime StartTime)
        {
            this.StartTime = StartTime;
            this.Complite = false;
            this.Comments = string.Empty;
        }
        public TimeStructure(DateTime StartTime, DateTime EndTime, string Comments = "")
        {
            this.StartTime = StartTime;
            this.EndTime = EndTime;
            this.Complite = true;
            this.Comments = Comments;
        }

        #region Реализация INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }

    /// <summary>
    /// Задачи общения с людми
    /// </summary>
    public class MyHuman : MyTask
    {

        public string ImagePath { get; }


        public MyHuman(string Name, string ImagePath, ObservableCollection<TimeStructure> Log) : base (Name, Log)
        {
            this.ImagePath = ImagePath;
        }
    }
    /// <summary>
    /// Задачи не учитывающиеся во времени
    /// </summary>
    public class MyRelaxationTask : MyTask
    {
        public MyRelaxationTask(string Name, ObservableCollection<TimeStructure> Log) : base(Name, Log) { }
    }
    /// <summary>
    /// Задачи записывающиеся в общение
    /// </summary>
    public class MyCommunicationTask : MyTask
    {
        public MyCommunicationTask(string Name, ObservableCollection<TimeStructure> Log) : base(Name, Log) { }
    }
    /// <summary>
    /// Выполняемые задачи
    /// </summary>
    public class MyTask : INotifyPropertyChanged
    {
        public string Name { get; }

        public ObservableCollection<TimeStructure> Log { get; }


        public bool StRun
        {
            get
            {
                return !(Log.Count == 0 || Log[Log.Count - 1].Complite);
            }
        }


        public void TimeRun()
        {
            if (this.Log.Count == 0 || this.Log[this.Log.Count - 1].Complite)
            {
                this.Log.Add(new TimeStructure(DateTime.Now));
            }
            else
            {
                if (this.Log[this.Log.Count - 1].Comments == "") {
                    var endtime = DateTime.Now;
                    CommentForm frm = new CommentForm(Name);
                    frm.ShowDialog();
                    this.Log[this.Log.Count - 1].End(endtime, frm.Comment != null ? frm.Comment : "");
                }
                else
                {
                    this.Log[this.Log.Count - 1].End(DateTime.Now);
                }
            }

            OnPropertyChanged(nameof(this.StRun));
        }


        public MyTask(string Name, ObservableCollection<TimeStructure> Log)
        {
            this.Name = Name;

            this.Log = Log;
        }


        #region Реализация INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        public void OnPropertyChanged([CallerMemberName]string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }





    ///// <summary>
    ///// Конвертер
    ///// </summary>
    //public class TimeStatusConverter : IValueConverter
    //{
    //    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        var Log = (ObservableCollection<TimeStructure>)value;
    //        return Log.Count == 0 || Log[Log.Count - 1].Complite;
    //        //return (int)value == 0;
    //    }


    //    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    //    {
    //        return null;
    //    }
    //}

    /// <summary>
    /// Конвертер
    /// </summary>
    public class TimeConverter : IMultiValueConverter
    {
        public object Convert(
            object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var el = (MyTask)values[1];
            var time = ((DateTime)values[0] - el.Log[el.Log.Count - 1].StartTime).Duration();
            string str="";
            if (time.Hours > 0) str = time.ToString(@"h\:mm\:ss");
            else if(time.Minutes > 0) str = time.ToString(@"m\:ss");
            else str = time.ToString(@"\ s");

            return values.Length == 3 ? str + " " + (string)values[2] : str;
        }

        public object[] ConvertBack(
            object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotSupportedException();
        }
    }


    /// <summary>
    /// Для комманд
    /// </summary>
    public class RelayActionCommand : ICommand
    {
        /// <summary>
        /// The Action Delegate representing a method with input parameter
        /// </summary>
        public Action<object> ExecuteAction { get; set; }
        /// <summary>
        /// The Delegate, used to represent the method which defines criteria for the execution
        /// </summary>
        public Predicate<object> CanExecuteAction { get; set; }
        public bool CanExecute(object parameter)
        {
            if (CanExecuteAction != null)
            {
                return CanExecuteAction(parameter);
            }
            return true;
        }
        /// <summary>
        /// The event which will be raised based upon the
        /// value set for the command.
        /// </summary>
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        public void Execute(object parameter)
        {
            ExecuteAction?.Invoke(parameter);
        }
    }
}
