﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace TimeTracker
{
    /// <summary>
    /// Логика взаимодействия для LogTimeProperty.xaml
    /// </summary>
    public partial class LogTimeProperty : Window
    {
        public RelayActionCommand DelCommand { get; set; }
        private void DelCommand_Execute(object obj)
        {
            var el = (TimeStructure)obj;
            thistask.Log.Remove(el);
        }
        private bool DelCommand_CanExecute(object obj)
        {
            if(obj!=null) return ((TimeStructure)obj).Complite;
            return false;
        }


        public MyTask thistask { get; }
        public LogTimeProperty(MyTask thistask)
        {
            //-команды
            DelCommand = new RelayActionCommand();
            DelCommand.CanExecuteAction += DelCommand_CanExecute;
            DelCommand.ExecuteAction += DelCommand_Execute;


            this.thistask = thistask;
            InitializeComponent();
        }
    }
}
