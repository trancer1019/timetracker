﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;


namespace TimeTracker
{
    /// <summary>
    /// Логика взаимодействия для CommentForm.xaml
    /// </summary>
    public partial class CommentForm : Window
    {
        public string Comment { get; set; }

        public CommentForm(string Name)
        {
            
            InitializeComponent();
            Title = "Комментарий к " + Name;
            //var windowPosition = Mouse.GetPosition(this);
            //var screenPosition = this.PointToScreen(windowPosition);
            //Left = 30;
            //Top = 30;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Comment = commenttb.Text;
        }
    }
}
